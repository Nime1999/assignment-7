#include <stdio.h>
#include <stdlib.h>

int main()
{
    char sentence[1000], rev[1000];
    int i, k, j;

    printf("Enter the sentence:");
    fgets(sentence,1000,stdin);

    for(i=0; sentence[i] != '\0'; i++ );
    j=i-1;

    for(k=0; k<=i; k++)
    {
        rev[k]=sentence[j];
        j--;
    }

    for(k=0; k<i; k++)
        printf("%c", rev[k]);
    return 0;
}
