#include <stdio.h>
#include <stdlib.h>

int main() {
    char str[1000], a;
    int count = 0;

    printf("Enter a string: ");
    fgets(str,1000, stdin);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &a);

    for (int i = 0; str[i] != '\0'; ++i) {
        if (a == str[i])
            ++count;
    }

    printf("Frequency of %c = %d",a, count);
    return 0;
}
